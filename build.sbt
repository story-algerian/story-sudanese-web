ThisBuild / name := "story-sudanese-web"
ThisBuild / version := "0.1.0"
ThisBuild / scalaVersion := "2.12.8"

scalacOptions ++= Seq(
  "-deprecation",
  "-feature",
  "-unchecked",
  "-Xfatal-warnings",
  "-Xlint:_",
  "-Ywarn-unused:-imports",
  "-Xverify",
)

lazy val root = (project in file(".")).enablePlugins(PlayScala)
  .dependsOn(engine)

lazy val engine = project in file("engine")

libraryDependencies ++= Seq(
  guice,
  "com.typesafe.play" %% "play-slick" % "4.0.0",
  "com.typesafe.play" %% "play-slick-evolutions" % "4.0.0",
  "org.postgresql" % "postgresql" % "42.2.5",
  "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.1" % Test,
  ws,
  "org.webjars" % "jquery" % "3.4.1",
  "org.webjars" % "popper.js" % "1.15.0",
  "org.webjars.npm" % "bs-custom-file-input" % "1.3.1",
  "org.webjars" % "font-awesome" % "5.8.2",
)
