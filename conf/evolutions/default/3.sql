-- Add the sample ADS2014 game

-- !Ups

ALTER TABLE users
ADD COLUMN prog BYTEA;

-- !Downs

ALTER TABLE users
DROP COLUMN prog;
