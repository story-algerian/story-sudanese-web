-- Add programs

-- !Ups

CREATE TABLE programs (
	id        SERIAL       NOT NULL,
	prog_name VARCHAR(256) NOT NULL,
	auth_id   INTEGER      NOT NULL,
	-- Might be a giant file, that's why it's just VARCHAR, with no
	-- bounds
	code      VARCHAR      NOT NULL,
	bytecode  BYTEA        NOT NULL,

	CONSTRAINT programs_pk    PRIMARY KEY (id),
	CONSTRAINT prog_name_uniq UNIQUE      (prog_name),
	CONSTRAINT auth_id_fk     FOREIGN KEY (auth_id) REFERENCES users(id)
	                          ON UPDATE CASCADE ON DELETE CASCADE
);

-- !Downs

DROP TABLE programs;
