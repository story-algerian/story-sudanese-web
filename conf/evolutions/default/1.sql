-- users schema
-- Had to edit this after commiting to git, not nice, but I'm a noob in SQL

-- !Ups
CREATE TABLE users (
  id    SERIAL      NOT NULL,
  uname VARCHAR(64) NOT NULL,
  email VARCHAR(64) NOT NULL,
  psalt BYTEA       NOT NULL,
  phash BYTEA       NOT NULL,

  CONSTRAINT users_pk     PRIMARY KEY (id),
  CONSTRAINT uname_unique UNIQUE      (uname),
  CONSTRAINT email_unique UNIQUE      (email)
);

-- !Downs
DROP TABLE users;
