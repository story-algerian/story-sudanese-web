-- alter users, add verification

-- !Ups
CREATE TABLE verifications (
  id      SERIAL NOT NULL,
  email   VARCHAR(64) NOT NULL,
  relpath VARCHAR(255) NOT NULL,
  expiry  DATE NOT NULL,

  CONSTRAINT verifications_pk PRIMARY KEY (id),
  CONSTRAINT vemail_unique    UNIQUE      (email),
  CONSTRAINT relpath_unique   UNIQUE      (relpath)
);

-- !Downs
DROP TABLE verifications;
