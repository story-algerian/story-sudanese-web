/* Constraints.scala -- Various constraints for data submitted in input forms.
 * Copyright (C) 2019  Uko Koknevics
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package util

import play.api.data.validation._

object Constraints {
  val atLeastOneUppercaseCheck: Constraint[String] = Constraint("constraint.uppercase")({
    plainText =>
        "[A-Z]".r.findFirstIn(plainText) match {
        case Some(_) => Valid
        case None => Invalid(ValidationError("constraint.uppercase"))
      }
  })

  val atLeastOneLowercaseCheck: Constraint[String] = Constraint("constraint.lowercase")({
    plainText =>
      "[a-z]".r.findFirstIn(plainText) match {
        case Some(_) => Valid
        case None => Invalid(ValidationError("constraint.lowercase"))
      }
  })

  val atLeastOneDigitCheck: Constraint[String] = Constraint("constraint.digit")({
    plainText =>
      "[0-9]".r.findFirstIn(plainText) match {
        case Some(_) => Valid
        case None => Invalid(ValidationError("constraint.digit"))
      }
  })

  val atLeastOnePunctuationCheck: Constraint[String] = Constraint("constraint.punctuation")({
    plainText =>
      """[ !"#$%&'()*+,\-./:;<=>?@\[\]\\^_`{|}~]""".r.findFirstIn(plainText) match {
        case Some(_) => Valid
        case None => Invalid(ValidationError("constraint.punctuation"))
      }
  })

  val noMoreThanTwoConsecutiveCheck: Constraint[String] = Constraint("constraint.consecutive")({
    plainText =>
      """(.)\1\1+""".r.findFirstIn(plainText) match {
        case Some(_) => Invalid(ValidationError("constraint.consecutive"))
        case None => Valid
      }
  })

  lazy val usernamePat = "^[A-Za-z0-9_]+$".r

  val usernameCheck: Constraint[String] = Constraint("constraint.username")({
    _ match {
      case usernamePat() => Valid
      case _ => Invalid(ValidationError("constraint.username"))
    }
  })

  lazy val passwordPat = """^[A-Za-z0-9 !"#$%&'()*+,\-./:;<=>?@\[\]\\^_`{|}~]+$""".r

  val passwordCheck: Constraint[String] = Constraint("constraint.password")({
    _ match {
      case passwordPat() => Valid
      case _ => Invalid(ValidationError("constraint.password"))
    }
  })
}
