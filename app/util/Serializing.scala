/* Serializing.scala -- nicer functions for serializing.
 * Copyright (C) 2019  Uko Koknevics
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package util

import java.io._

object Serializing {
  def deser[A <: Serializable](arr: Array[Byte]): Option[A] = if (arr == null) {
      None
  } else {
    try {
      val bais = new ByteArrayInputStream(arr)
      val ois = new ObjectInputStream(bais)
      val ret = ois.readObject().asInstanceOf[A]
      ois.close()
      Some(ret)
    } catch {
      case _: InvalidClassException =>
        // SerialVersionUID mismatched, progress lost
        // TODO: let the user know
        None
    }
  }

  def ser[A <: Serializable](o: Option[A]): Array[Byte] = o match {
    case None =>
      null

    case Some(o) =>
      val baos = new ByteArrayOutputStream
      val oos = new ObjectOutputStream(baos)
      oos.writeObject(o)
      oos.close()
      baos.toByteArray
  }
}
