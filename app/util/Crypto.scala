/* Crypto.scala -- just hashing the password for now.
 * Copyright (C) 2019  Uko Koknevics
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package util

import java.nio.charset.Charset
import java.security.{MessageDigest, SecureRandom}

object Crypto {
  lazy val md = MessageDigest.getInstance("SHA-1")
  lazy val rand = SecureRandom.getInstanceStrong()
  var randUsage = 0

  def newByteArray(size: Int): Array[Byte] = {
    randUsage += 1
    if (randUsage > 2000) {
      randUsage = 0
      rand.setSeed(rand.generateSeed(20))
    }

    val ret = new Array[Byte](size)
    rand.nextBytes(ret)
    ret
  }

  def newSalt: Array[Byte] = newByteArray(64)

  def hash(salt: Array[Byte], pass: String): Array[Byte] =
      md.digest(salt ++ pass.getBytes(Charset.forName("UTF-8")))
}
