/* VerificationDAO.scala -- Database Access Object for Verification model.
 * Copyright (C) 2019  Uko Koknevics
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package dao

import java.time._
import javax.xml.bind.DatatypeConverter
import javax.inject._
import models._
import play.api.db.slick._
import scala.concurrent._
import slick.jdbc._
import util.Crypto

class VerificationDAO @Inject()(
  protected val dbConfigProvider: DatabaseConfigProvider
  )(implicit ec: ExecutionContext)
  extends HasDatabaseConfigProvider[PostgresProfile] {

  import profile.api._

  private val verifications = TableQuery[VerificationTable]

  def all(): Future[Seq[Verification]] = db.run(verifications.result)

  def delete(v: Verification): Future[Unit] =
    db.run(verifications.filter(_.id === v.id).delete).map { _ => {}}

  def existsEmail(email: String): Future[Boolean] = getByEmail(email).map {
    _ match {
      case None => false
      case Some(_) => true
    }
  }

  def existsRelpath(relpath: String): Future[Boolean] = getByRelpath(relpath).map {
    _ match {
      case None => false
      case Some(_) => true
    }
  }

  def generateRelpath: Future[String] = {
    val relpath = DatatypeConverter.printHexBinary(Crypto.newByteArray(100))
    existsRelpath(relpath).flatMap {
      if (_) {
        generateRelpath
      } else {
        Future { relpath }
      }
    }
  }

  def generateVerification(email: String): Future[Verification] =
    generateRelpath.map { relpath =>
      Verification(0, email, relpath, LocalDate.now().plusDays(3))
    }

  def getByEmail(email: String): Future[Option[Verification]] = {
    val q = verifications.filter(_.email.toLowerCase === email.toLowerCase)
    db.run(q.filter(_.expiry < LocalDate.now()).delete).flatMap { _ =>
      db.run(q.result.headOption)
    }
  }

  def getByRelpath(relpath: String): Future[Option[Verification]] = {
    val q = verifications.filter(_.relpath === relpath)
    db.run(q.filter(_.expiry < LocalDate.now()).delete).flatMap { _ =>
      db.run(q.result.headOption)
    }
  }

  def insert(v: Verification): Future[Unit] =
    db.run(verifications += v).map { _ => {}}

  private class VerificationTable(tag: Tag) extends Table[Verification](tag, "verifications") {
    def id      = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def email   = column[String]("email", O.Unique)
    def relpath = column[String]("relpath", O.Unique)
    def expiry  = column[LocalDate]("expiry")

    def * = (id, email, relpath, expiry) <> (Verification.tupled, Verification.unapply)
  }
}
