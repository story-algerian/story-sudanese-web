/* UserDAO.scala -- Database access object for User model
 * Copyright (C) 2019  Uko Koknevics
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package dao

import javax.inject.Inject
import models.{User, WebExecutor}
import models.WebExecutor.WebExecutorJdbcType
import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfigProvider
import scala.concurrent.{ExecutionContext, Future}
import slick.jdbc.PostgresProfile

class UserDAO @Inject() (
  protected val dbConfigProvider: DatabaseConfigProvider
  )(implicit ec: ExecutionContext)
  extends HasDatabaseConfigProvider[PostgresProfile] {

  import profile.api._

  private[dao] val users = TableQuery[UserTable]

  def all(): Future[Seq[User]] = db.run(users.result)

  def existsUname(uname: String): Future[Boolean] =
    db.run(users.filter(_.uname.toLowerCase === uname.toLowerCase).exists.result)

  def existsEmail(email: String): Future[Boolean] =
    db.run(users.filter(_.email.toLowerCase === email.toLowerCase).exists.result)

  def getById(id: Int): Future[Option[User]] =
    db.run(users.filter(_.id === id).result.headOption)

  def getByUname(uname: String): Future[Option[User]] =
    db.run(users.filter(_.uname.toLowerCase === uname.toLowerCase).result.headOption)

  def insert(u: User): Future[Unit] = db.run(users += u).map { _ => {} }

  def updateProg(u: User, exe: Option[WebExecutor]): Future[Unit] =
    db.run(users.filter(_.id === u.id).map { _.prog }.update(exe)).map { _ => {} }

  private[dao] class UserTable(tag: Tag) extends Table[User](tag, "users") {
    def id    = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def uname = column[String]("uname", O.Unique)
    def email = column[String]("email", O.Unique)
    def psalt = column[Array[Byte]]("psalt")
    def phash = column[Array[Byte]]("phash")
    def prog  = column[Option[WebExecutor]]("prog")

    def * = (id, uname, email, psalt, phash, prog) <> (User.tupled, User.unapply)
  }
}
