/* UserDAO.scala -- Database access object for User model
 * Copyright (C) 2019  Uko Koknevics
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package dao

import javax.inject.Inject
import models.{Program, ProgramInfo, User, WebExecutor}
import models.WebExecutor.WebExecutorJdbcType
import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfigProvider
import scala.concurrent.{ExecutionContext, Future}
import slick.jdbc.PostgresProfile

class ProgramDAO @Inject() (
  protected val dbConfigProvider: DatabaseConfigProvider,
  userDao: UserDAO
)(implicit ec: ExecutionContext)
  extends HasDatabaseConfigProvider[PostgresProfile] {

  import profile.api._

  private val programs = TableQuery[ProgramTable]

  def all(): Future[Seq[Program]] = db.run(programs.result)

  def allInfo(): Future[Seq[ProgramInfo]] =
    db.run((for {
      p <- programs
      author <- p.auth
    } yield (p, author)).map { case (p, author) =>
        (p.id, p.progName, author)
    }.sortBy(_._1).result.map { _.reverse.map { ProgramInfo.tupled }})

  def getByName(progName: String): Future[Option[Program]] =
    db.run(programs.filter(_.progName === progName).result.headOption)

  def insert(p: Program): Future[Unit] = db.run(programs += p).map { _ => {} }

  def updateByteCode(p: Program, exe: Option[WebExecutor]): Future[Unit] =
    db.run(programs.filter(_.id === p.id).map { _.bytecode }.update(exe)).map { _ => {} }

  private class ProgramTable(tag: Tag) extends Table[Program](tag, "programs") {
    def id       = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def progName = column[String]("prog_name", O.Unique)
    def authId   = column[Int]("auth_id")
    def code     = column[String]("code")
    def bytecode = column[Option[WebExecutor]]("bytecode")

    def * = (id, progName, authId, code, bytecode) <> (Program.tupled, Program.unapply)
    def info = (id, progName, authId)
    def auth = foreignKey("auth_id_fk", authId, userDao.users)(_.id)
  }
}
