/* User.scala -- User model.
 * Copyright (C) 2019  Uko Koknevics
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package models

import util.Crypto

case class User(
  val id:    Int,
  val uname: String,
  val email: String,
  val psalt: Array[Byte],
  val phash: Array[Byte],
  val prog:  Option[WebExecutor]
)

object User {
  def apply(uname: String, email: String, psalt: Array[Byte], phash: Array[Byte]): User =
    User(0, uname, email, psalt, phash, None)

  def tupled(t: (Int, String, String, Array[Byte], Array[Byte],
                 Option[WebExecutor])): User =
    User(t._1, t._2, t._3, t._4, t._5, t._6)
}

case class RegisterUser(
  val uname: String,
  val email: String,
  val pword: String
)

case class LoginUser(
  val uname: String,
  val pword: String
) {
  def hash(email: String): User = {
    val psalt = Crypto.newSalt
    val phash = Crypto.hash(psalt, pword)
    User(uname, email, psalt, phash)
  }
}
