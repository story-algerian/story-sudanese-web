/* Program.scala -- Program model.
 * Copyright (C) 2019  Uko Koknevics
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package models

case class Program(
  val id:        Int,
  val prog_name: String,
  val auth_id:   Int,
  // Might want to remove this if I allow giant files.
  val code:      String,
  val bytecode:  Option[WebExecutor]
)

object Program {
  def apply(prog_name: String, auth_id: Int, code: String,
            bytecode: Option[WebExecutor]): Program =
    Program(0, prog_name, auth_id, code, bytecode)

  def tupled(t: (Int, String, Int, String, Option[WebExecutor])): Program =
    Program(t._1, t._2, t._3, t._4, t._5)
}

case class ProgramInfo(
  val id:        Int,
  val prog_name: String,
  val author:    User
)
