/* WebExecutor.scala -- an implementation of story-sudanese engine executor.
 * Copyright (C) 2019  Uko Koknevics
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package models

import enumeratum._
import java.sql.{PreparedStatement, ResultSet}
import scala.collection.mutable.ArrayBuffer
import scala.util.Random
import slick.ast.FieldSymbol
import slick.jdbc.PostgresProfile.DriverJdbcType
import ukkf.sudanese.ast
import ukkf.sudanese.ast.block.CodeBlock
import ukkf.sudanese.exe.Executor
import ukkf.sudanese.io.{Color, Logger}
import ukkf.sudanese.io.err.{ChoiceQueueEmpty, RuntimeError}
import ukkf.sudanese.vars.Variables
import util.Serializing

sealed trait State extends EnumEntry

object State extends Enum[State] {
  val values = findValues

  case object Running extends State
  case object BadEnd  extends State
  case object GoodEnd extends State
  case object Failure extends State
}

final object WebExecutor {
  implicit object WebExecutorJdbcType extends DriverJdbcType[Option[WebExecutor]] {
    // def sqlType = java.sql.Types.BLOB
    def sqlType = java.sql.Types.BINARY // POSTGRES
    override def sqlTypeName(sym: Option[FieldSymbol]) = "BYTEA" // POSTGRES
    def setValue(v: Option[WebExecutor], p: PreparedStatement, idx: Int) =
      p.setBytes(idx, Serializing.ser(v))
    def getValue(r: ResultSet, idx: Int) = Serializing.deser(r.getBytes(idx))
    def updateValue(v: Option[WebExecutor], r: ResultSet, idx: Int) =
      r.updateBytes(idx, Serializing.ser(v))
    override def hasLiteralForm = false
  }
}

@SerialVersionUID(201L)
final class WebExecutor(prog: ast.prog.Program) extends Executor with Serializable {
  private case class EndException(val good: Boolean) extends Exception
  private case object ChooseException extends Exception

  private var choices: Map[String, CodeBlock] = Map()
  private var currentBlock: Option[CodeBlock] = Some(prog.initBlock)
  private var _state: State = State.Running
  private var _speed: Long = 0
  private var ret: (StringBuilder, ArrayBuffer[String]) =
    (new StringBuilder, new ArrayBuffer)

  override def vars = prog.vars

  override def speed: Long = _speed
  override def speed_=(sp: BigInt): Unit = { _speed = sp.toLong }

  def execute(log: Logger): Option[(String, List[String])] = {
    currentBlock.map { block =>
      try {
        block.execute(this, log) match {
          case None => return None
          case Some(_) => {}
        }
      } catch {
        case EndException(end) => state = if (end) State.GoodEnd else State.BadEnd
        case ChooseException => {}
        case _: StackOverflowError =>
          state = State.Failure
          val sb = new StringBuilder
          sb ++= "Stack Overflow (Did you try to make an infinite loop?)"
          ret = (sb, new ArrayBuffer)
      }

      currentBlock = None
    }

    Some((ret._1.toString, ret._2.toList))
  }

  private def state_=(s: State) = _state = s
  def state: State = _state

  def makeChoice(choice: String): Option[Unit] = choices.get(choice).map { cb =>
    choices = Map()
    ret = (new StringBuilder, new ArrayBuffer)
    currentBlock = Some(cb)
  }

  override def addChoice(display: String, target: CodeBlock, log: Logger): Option[Unit] =
    Some(choices = choices + (display -> target))

  override def badEnd(log: Logger): Option[Unit] = throw EndException(false)

  override def choose(log: Logger): Option[Unit] = if (choices.size == 0) {
    log.err(-1, None, RuntimeError(ChoiceQueueEmpty))
    return None
  } else {
    choices.keys.foreach { key =>
      ret._2.append(key)
    }

    throw ChooseException
  }

  override def chooseRandom(log: Logger): Option[Unit] = if (choices.size == 0) {
    log.err(-1, None, RuntimeError(ChoiceQueueEmpty))
    return None
  } else {
    var values = choices.values
    values = values.drop(Random.nextInt(choices.size))
    choices = Map()
    values.head.execute(this, log)
  }

  override def clearScreen(log: Logger): Option[Unit] = Some(()) // TODO

  override def goodEnd(log: Logger): Option[Unit] = throw EndException(true)

  override def output(text: String, log: Logger): Option[Unit] =
    Some(ret._1.append(s"${text}\n"))

  override def sleep(time: BigInt, log: Logger): Option[Unit] = Some(()) // TODO

  override def useColor(col: Color, log: Logger): Option[Unit] = Some(()) // TODO

  override def waitLine(log: Logger): Option[Unit] = Some(()) // TODO
}
