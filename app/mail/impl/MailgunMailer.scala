/* MailgunMailer.scala -- Mailer implementation with Mailgun backend.
 * Copyright (C) 2019  Uko Koknevics
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package mail.impl

import javax.inject._
import mail._
import play.api._
import play.api.libs.ws._
import scala.concurrent._
import scala.concurrent.duration._
import scala.util._

@Singleton
class MailgunMailer @Inject()(ws: WSClient)(implicit ec: ExecutionContext) extends Mailer with Logging {
  lazy val apiKey = sys.env("MAILGUN_API_KEY")
  lazy val baseUrl = s"https://api.mailgun.net/v3/${sys.env("MAILGUN_DOMAIN")}/"

  def sendMail(to: Seq[String], from: String, subject: String, text: String, tags: Seq[String]): Future[Try[Unit]] =
    ws.url(s"${baseUrl}messages")
      .withAuth("api", apiKey, WSAuthScheme.BASIC)
      .withFollowRedirects(true)
      .withRequestTimeout(10.seconds)
      .post(Map(
        "to" -> to,
        "from" -> Seq(from),
        "subject" -> Seq(subject),
        "text" -> Seq(text),
        "o:tag" -> tags
      )).map { res =>
        if (res.status / 100 == 2) {
          logger.info(s"Sent email: ${res.status} ${res.statusText}")
          logger.info(res.body)
          Success(())
        } else {
          logger.error(s"Failed to send email: ${res.status} ${res.statusText}")
          logger.error(res.body)
          Failure(new Exception(s"${res.status} ${res.statusText}"))
        }
      }

  def sendTemplated(to: Seq[String], from: String, subject: String, template: String, templateVars: Map[String, String], tags: Seq[String]): Future[Try[Unit]] =
    ws.url(s"${baseUrl}messages")
      .withAuth("api", apiKey, WSAuthScheme.BASIC)
      .withFollowRedirects(true)
      .withRequestTimeout(10.seconds)
      .post(templateVars.foldLeft(Map(
          "to" -> to,
          "from" -> Seq(from),
          "subject" -> Seq(subject),
          "template" -> Seq(template),
          "o:tag" -> tags
        )) { (m, tvar) => m + (s"v:${tvar._1}" -> Seq(tvar._2)) }
      ).map { res =>
        if (res.status / 100 == 2) {
          logger.info(s"Sent email: ${res.status} ${res.statusText}")
          logger.info(res.body)
          Success(())
        } else {
          logger.error(s"Failed to send email: ${res.status} ${res.statusText}")
          logger.error(res.body)
          Failure(new Exception(s"${res.status} ${res.statusText}"))
        }
      }
}
