/* Mailer.scala -- Mailer trait.
 * Copyright (C) 2019  Uko Koknevics
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package mail

import com.google.inject._
import mail.impl._
import scala.concurrent._
import scala.util._

@ImplementedBy(classOf[MailgunMailer])
trait Mailer {
  def sendMail(to: Seq[String], from: String, subject: String, text: String, tags: Seq[String]): Future[Try[Unit]]
  def sendTemplated(to: Seq[String], from: String, subject: String, template: String, templateVars: Map[String, String], tags: Seq[String]): Future[Try[Unit]]
}
