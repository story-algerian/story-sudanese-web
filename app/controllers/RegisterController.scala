/* RegisterController.scala -- /register
 * Copyright (C) 2019  Uko Koknevics
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package controllers

import dao.{UserDAO, VerificationDAO}
import javax.inject._
import mail.Mailer
import models.RegisterUser
import play.api.data._
import play.api.data.Forms._
import play.api.data.validation._
import play.api.data.validation.Constraints._
import play.api.mvc._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}
import util.Constraints._

class RegisterController @Inject()(
  mcc: MessagesControllerComponents,
  userDao: UserDAO,
  verificationDao: VerificationDAO,
  mailer: Mailer
  )(implicit ec: ExecutionContext) extends MessagesAbstractController(mcc) {

  val registerForm = Form(
    single("email" -> email)
  )

  def get = Action { implicit req =>
    req.session.get("connected").map { _ =>
      Redirect(routes.HomeController.index())
    }.getOrElse {
      Ok(views.html.register(registerForm))
    }
  }

  def post = Action.async { implicit req =>
    req.session.get("connected").map { _ =>
      Future { Redirect(routes.HomeController.index()) }
    }.getOrElse {
      val form = registerForm.bindFromRequest
      form.fold(
        formWithErrors => {
          Future { BadRequest(views.html.register(formWithErrors)) }
        },
        email => {
          userDao.existsEmail(email).flatMap {
            if (_) {
              Future { BadRequest(views.html.register(form.withError("email", "taken.email"))) }
            } else {
              verificationDao.existsEmail(email).flatMap {
                if (_) {
                  Future {
                    Redirect(routes.HomeController.index()).flashing("verificationSent" -> "")
                  }
                } else {
                  verificationDao.generateVerification(email).flatMap {
                    ver =>
                    mailer.sendTemplated(
                      to = Seq(email),
                      from = "Story Sudanese <no-reply@sudanese.bestest.id.lv>",
                      subject = "Email verification",
                      template = "verify.template",
                      templateVars = Map("link" -> s"${req.host}/verify/${ver.relpath}"),
                      tags = Seq("validation")
                    ).flatMap {
                      _ match {
                        case Failure(_) => Future {
                          Redirect(routes.HomeController.index())
                            .flashing("failedToSendVerification" -> "")
                        }
                        case Success(_) =>
                          verificationDao.insert(ver).map { _ =>
                            Redirect(routes.HomeController.index())
                              .flashing("verificationSent" -> "")
                          }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      )
    }
  }
}
