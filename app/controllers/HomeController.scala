/* HomeController.scala -- /index, /license, /list
 * Copyright (C) 2019  Uko Koknevics
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package controllers

import dao.{ProgramDAO, UserDAO}
import javax.inject._
import play.api.mvc._
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class HomeController @Inject()(
  mcc: MessagesControllerComponents,
  programDao: ProgramDAO,
  userDao: UserDAO
  )(implicit ec: ExecutionContext) extends MessagesAbstractController(mcc) {

  def index = Action.async { implicit req =>
    req.session.get("connected").map { uname =>
      userDao.getByUname(uname).map {
        _ match {
          case None => Ok(views.html.index()).withNewSession
          case Some(u) => Ok(views.html.index())
        }
      }
    }.getOrElse {
      Future { Ok(views.html.index()) }
    }
  }

  def license = Action { implicit req => Ok(views.html.license()) }

  def list = Action.async { implicit req =>
    programDao.allInfo().map { list =>
      Ok(views.html.list(list))
    }
  }
}
