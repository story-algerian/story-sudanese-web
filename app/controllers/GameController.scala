/* GameController.scala -- /game
 * Copyright (C) 2019  Uko Koknevics
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package controllers

import dao.{ProgramDAO, UserDAO}
import java.net.{URLDecoder, URLEncoder}
import javax.inject._
import models.{State, User, WebExecutor}
import play.api.mvc._
import scala.concurrent.{ExecutionContext, Future}
import scala.io.Source
import ukkf.sudanese.ast.prog.ProgramBuilder
import ukkf.sudanese.io.Logger

// FIXME: When having two equal choices in code, they are displayed as
// one choice in the game.  Example:
//
// > You selected "YES", is that right?
// - YES -> yes
// - YES -> yes
// Choose
//
// Appears as
//
// You selected "YES", is that right?
// [YES]

class GameController @Inject()(
  mcc: MessagesControllerComponents,
  programDao: ProgramDAO,
  userDao: UserDAO
  )(implicit ec: ExecutionContext) extends MessagesAbstractController(mcc) {

  private def runProg(user: User, exe: WebExecutor)
                     (implicit req: MessagesRequest[AnyContent]) = {
    val log = Logger("")
    exe.execute(log) match {
      case None => userDao.updateProg(user, None).map { _ =>
        InternalServerError(views.html.game(
          s"Something went wrong, probably a mistake in game code\n\n${log}",
          ("../restart", "Restart") :: Nil))
      }
      case Some((text, choices)) => userDao.updateProg(user, Some(exe)).map { _ =>
        val choices2 = choices.map(c => (URLEncoder.encode(c, "UTF-8"), c))
        if (!choices2.isEmpty) {
          Ok(views.html.game(text, choices2))
        } else {
          val text2 = exe.state match {
            case State.Running =>
              "No choices available to display, probably a bug in the game code."

            case State.BadEnd =>
              "You lost the game!"

            case State.GoodEnd =>
              "You won the game!"

            case State.Failure =>
              "Failure, possibly in game code, can't continue"
          }

          Ok(views.html.game(s"${text}\n\n${text2}", Nil))
        }
      }
    }
  }

  def get = Action.async { implicit req =>
    req.session.get("connected").map { uname =>
      userDao.getByUname(uname).flatMap { _ match {
        case None =>
          Future {
            Redirect(routes.HomeController.index())
              .withNewSession
              .flashing("relogin" -> "a")
          }

        case Some(user) => user.prog match {
          case None =>
            Future {
              Redirect(routes.HomeController.list())
                .flashing("noGameStarted" -> "a")
            }

          case Some(exe) =>
            runProg(user, exe)
        }
      }}
    }.getOrElse {
      Future {
        Redirect(routes.HomeController.index())
          .flashing("notLoggedIn" -> "a")
      }
    }
  }

  def choose(choice: String) = Action.async { implicit req =>
    req.session.get("connected").map { uname =>
      userDao.getByUname(uname).flatMap { _ match {
        case None => // TODO redirect to main page
          Future {}

        case Some(user) =>
          user.prog match {
            case None =>
              Future {}

            case Some(exe) =>
              exe.makeChoice(URLDecoder.decode(choice, "UTF-8"))
              userDao.updateProg(user, Some(exe))
          }
      }}.map { _ =>
        // TODO: I don't have to redirect, I could update prog right here
        Redirect(routes.GameController.get())
      }
    }.getOrElse {
      Future {
        Redirect(routes.HomeController.index())
          .flashing("notLoggedIn" -> "a")
      }
    }
  }

  def select(gameUrl: String) = Action.async { implicit req =>
    val gameName = URLDecoder.decode(gameUrl, "UTF-8")
    req.session.get("connected").map { uname =>
      userDao.getByUname(uname).flatMap { _ match {
        case None =>
          Future {
            Redirect(routes.HomeController.index())
              .flashing("notLoggedIn" -> "a")
          }

        case Some(user) =>
          programDao.getByName(gameName).flatMap { _ match {
            case None =>
              Future {
                Redirect(routes.HomeController.list())
                  .flashing("gameNotFound" -> "a")
              }

            case Some(prog) =>
              prog.bytecode match {
                case None =>
                  val log = Logger(gameName)
                  ProgramBuilder.buildProg(Source.fromString(prog.code), log) match {
                    case None =>
                        // TODO delete the prog
                        // TODO error
                        Future {
                          InternalServerError(views.html.err.err500(log.toString))
                        }

                    case Some(p) => // TODO: Warnings
                      val exe = new WebExecutor(p)
                      programDao.updateByteCode(prog, Some(exe))
                      userDao.updateProg(user, Some(exe)).map { _ =>
                        Redirect(routes.GameController.get())
                      }
                  }

                case Some(exe) =>
                  userDao.updateProg(user, Some(exe)).map { _ =>
                    Redirect(routes.GameController.get())
                  }
              }
          }}
      }}
    }.getOrElse {
      Future { Redirect(routes.GameController.get()) }
    }
  }
}
