/* LoginController.scala -- /login
 * Copyright (C) 2019  Uko Koknevics
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package controllers

import dao.UserDAO
import models.LoginUser
import util.Constraints._
import util.Crypto

import java.util.Arrays
import javax.inject._
import play.api.data._
import play.api.data.Forms._
import play.api.data.validation._
import play.api.data.validation.Constraints._
import play.api.mvc._
import scala.concurrent.{ExecutionContext, Future}

class LoginController @Inject()(
  mcc: MessagesControllerComponents,
  userDao: UserDAO
  )(implicit ec: ExecutionContext) extends MessagesAbstractController(mcc) {

  val loginForm = Form(
    mapping(
      "uname" -> text(minLength = 4, maxLength = 64)
                .verifying(usernameCheck),
      "pword" -> nonEmptyText(minLength = 10, maxLength = 128)
                .verifying(atLeastOneUppercaseCheck, atLeastOneLowercaseCheck,
                           atLeastOneDigitCheck, atLeastOnePunctuationCheck,
                           noMoreThanTwoConsecutiveCheck, passwordCheck)
    )(LoginUser.apply)(LoginUser.unapply)
  )

  def get = Action { implicit req =>
    req.session.get("connected").map { _ =>
      Redirect(routes.HomeController.index())
    }.getOrElse {
      Ok(views.html.login(loginForm))
    }
  }

  def post = Action.async { implicit req =>
    req.session.get("connected").map { _ =>
      Future { Redirect(routes.HomeController.index()) }
    }.getOrElse {
      val form = loginForm.bindFromRequest
      form.fold(
        formWithErrors => {
          Future { BadRequest(views.html.login(formWithErrors.discardingErrors.withGlobalError("login.incorrect"))) }
        },
        userData => {
          userDao.getByUname(userData.uname).map { _ match {
              case Some(user) =>
                val hash = Crypto.hash(user.psalt, userData.pword)
                if (Arrays.equals(hash, user.phash)) {
                  Redirect(routes.HomeController.index()).withSession(
                    req.session + ("connected" -> user.uname)
                  )
                } else {
                  BadRequest(views.html.login(form.withGlobalError("login.incorrect")))
                }

              case None => BadRequest(views.html.login(form.withGlobalError("login.incorrect")))
            }
          }
        }
      )
    }
  }
}
