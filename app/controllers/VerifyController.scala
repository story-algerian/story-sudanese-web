/* VerifiedController.scala -- /verify
 * Copyright (C) 2019  Uko Koknevics
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package controllers

import dao.{UserDAO, VerificationDAO}
import models.LoginUser
import util.Constraints._

import javax.inject._
import play.api.data._
import play.api.data.Forms._
import play.api.data.validation._
import play.api.data.validation.Constraints._
import play.api.mvc._
import scala.concurrent.{ExecutionContext, Future}

class VerifyController @Inject()(
  mcc: MessagesControllerComponents,
  userDao: UserDAO,
  verificationDao: VerificationDAO
  )(implicit ec: ExecutionContext)
  extends MessagesAbstractController(mcc) {

  val registerForm = Form(
    mapping(
      "uname" -> text(minLength = 4, maxLength = 64)
                .verifying(usernameCheck),
      "pword" -> text(minLength = 10, maxLength = 128)
                .verifying(atLeastOneUppercaseCheck, atLeastOneLowercaseCheck,
                           atLeastOneDigitCheck, atLeastOnePunctuationCheck,
                           noMoreThanTwoConsecutiveCheck, passwordCheck)
    )(LoginUser.apply)(LoginUser.unapply)
  )

  def get(link: String) = Action.async { implicit req =>
    verificationDao.existsRelpath(link).map {
      if (_) {
        req.session.get("connected").map { _ =>
          Redirect(routes.HomeController.index())
        }.getOrElse {
          Ok(views.html.verify(registerForm, link))
        }
      } else {
        Redirect(routes.RegisterController.get()).flashing("linkNotFound" -> "")
      }
    }
  }

  def post(link: String) = Action.async { implicit req =>
    req.session.get("connected").map { _ =>
      Future { Redirect(routes.HomeController.index()) }
    }.getOrElse {
      verificationDao.getByRelpath(link).flatMap {
        _ match {
          case None => Future {
            Redirect(routes.RegisterController.get()).flashing("linkNotFound" -> "")
          }
          case Some(ver) =>
            val form = registerForm.bindFromRequest
            form.fold(
              formWithErrors => {
                Future { BadRequest(views.html.verify(formWithErrors, link)) }
              },
              userData => {
                val email = ver.email
                val uname = userData.uname
                userDao.existsUname(uname).flatMap {
                  if (_) {
                    Future {
                      BadRequest(
                        views.html.verify(form.withError("uname", "taken.uname"), link))
                    }
                  } else {
                    userDao.insert(userData.hash(email)).flatMap { _ =>
                      verificationDao.delete(ver)
                    }.map { _ =>
                      Redirect(routes.HomeController.index())
                        .withSession("connected" -> uname)
                    }
                  }
                }
              }
            )
        }
      }
    }
  }
}
