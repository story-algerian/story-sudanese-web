/* UploadController.scala -- /upload
 * Copyright (C) 2019  Uko Koknevics
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package controllers

import dao.{ProgramDAO, UserDAO}
import java.nio.file.{Files, Paths}
import javax.inject._
import models.{Program, User, WebExecutor}
import play.api.data._
import play.api.data.Forms._
import play.api.data.validation.Constraints._
import play.api.mvc._
import scala.concurrent.{ExecutionContext, Future}
import scala.io.Source
import ukkf.sudanese.ast.prog.ProgramBuilder
import ukkf.sudanese.io.Logger

class UploadController @Inject()(
  mcc: MessagesControllerComponents,
  programDao: ProgramDAO,
  userDao: UserDAO
)(implicit ec: ExecutionContext) extends MessagesAbstractController(mcc) {

  val uploadForm = Form(
    single("name" -> nonEmptyText(4, 128))
  )

  def get = Action.async { implicit req =>
    req.session.get("connected").map { uname =>
      userDao.getByUname(uname).map { _ match {
        case None =>
          Redirect(routes.HomeController.index())
            .withNewSession
            .flashing("relogin" -> "a")

        case Some(user) =>
          Ok(views.html.upload(uploadForm))
      }}
    }.getOrElse {
      Future {
        Redirect(routes.HomeController.index())
          .flashing("notLoggedIn" -> "a")
      }
    }
  }

  def post = Action.async(parse.multipartFormData) { implicit req =>
    req.session.get("connected").map { uname =>
      userDao.getByUname(uname).flatMap { _ match {
        case None =>
          Future {
            Redirect(routes.HomeController.index())
              .withNewSession
              .flashing("relogin" -> "a")
          }

        case Some(user) =>
          val form = uploadForm.bindFromRequest
          form.fold(
            formWithErrors => {
              Future { BadRequest(views.html.upload(formWithErrors)) }
            },
            name => {
              req.body.file("code").map { codeFile =>
                val fileSize = codeFile.fileSize
                if (fileSize > 1024 * 1024) {
                  Future {
                    BadRequest(
                      views.html.upload(form.withGlobalError("error.fileTooBig")))
                  }
                } else {
                  val pubName = s"${uname}/${name}"
                  println(s"Creating a program at '${pubName}'")
                  val data = new String(Files.readAllBytes(codeFile.ref), "UTF-8")
                  val log = Logger(pubName)
                  ProgramBuilder.buildProg(Source.fromString(data), log) match {
                    case None =>
                      Future {
                        BadRequest(views.html.upload(
                          form
                            .withGlobalError("error.failedBuild")
                            .withGlobalError(log.toString)))
                      }

                    case Some(prog) =>
                      val exe = new WebExecutor(prog)
                      val db = Program(pubName, user.id, data, Some(exe))
                      programDao.insert(db).map { _ =>
                        // TODO: Give info about success
                        // TODO: Give info about warnings
                        Redirect(routes.HomeController.list())
                      }
                  }
                }
              }.getOrElse {
                Future {
                  BadRequest(
                    views.html.upload(form.withGlobalError("error.fileNotReceived")))
                }
              }
            }
          )
      }}
    }.getOrElse {
      Future {
        Redirect(routes.HomeController.index())
          .flashing("notLoggedIn" -> "a")
      }
    }
  }
}
