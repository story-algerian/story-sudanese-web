/* ErrorHandler.scala -- HTTP error handler.
 * Copyright (C) 2019  Uko Koknevics
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

import javax.inject._
import play.api._
import play.api.http.DefaultHttpErrorHandler
import play.api.i18n.{Lang, Messages, MessagesApi, MessagesImpl}
import play.api.mvc._
import play.api.mvc.Results._
import play.api.routing.Router
import scala.concurrent._

@Singleton
class ErrorHandler @Inject() (
    env: Environment,
    config: Configuration,
    messagesApi: MessagesApi,
    router: Provider[Router],
    sourceMapper: OptionalSourceMapper
  ) extends DefaultHttpErrorHandler(env, config, sourceMapper, router) {

  val msg: Messages = MessagesImpl(Lang("en"), messagesApi)

  override def onBadRequest(req: RequestHeader, message: String): Future[Result] =
    Future.successful {
      BadRequest(views.html.err.err400(message)(req, msg))
    }

  override def onForbidden(req: RequestHeader, message: String): Future[Result] =
    Future.successful {
      Forbidden(views.html.err.err403(req.path, message)(req, msg))
    }

  override def onNotFound(req: RequestHeader, message: String): Future[Result] =
    Future.successful {
      NotFound(views.html.err.err404(req.path, message)(req, msg))
    }

  override def onOtherClientError(req: RequestHeader, statusCode: Int, message: String): Future[Result] =
    Future.successful {
      Status(statusCode)(views.html.err.err4xx(statusCode, message)(req, msg))
    }


  override def onProdServerError(req: RequestHeader, exception: UsefulException): Future[Result] =
    Future.successful {
      val sb = new StringBuilder
      sb ++= exception.toString
      val stack = exception.getStackTrace()
      if (stack.length > 0) {
        val info = stack.head
        sb ++= s"\n${info.getFileName()}:${info.getLineNumber()}"
      }

      InternalServerError(views.html.err.err500(sb.toString)(req, msg))
    }
}
