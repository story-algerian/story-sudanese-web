addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.7.0")
addSbtPlugin("com.thoughtworks.sbt-api-mappings" % "sbt-api-mappings" % "3.0.0")
//disabling this plugin because it's architecture dependant
//addSbtPlugin("org.irundaia.sbt" % "sbt-sassify" % "1.4.13")
